import React from 'react';
import { connect, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { selectCollection } from '../../redux/shop/shop.selectors';
import CollectionItem from '../../components/collection-item/collection-item.component';
import './collection.styles.scss';
const CollectionPage = () => {

    const {collectionId} = useParams();
    const collection = useSelector(selectCollection(collectionId))
    const {title, items} = collection;
    return (
        <div className="collection-page">
            <h2>{title}</h2>
            <div className="items">
                {
                    items.map(item => (
                        <CollectionItem key={item.id} item={item}/>
                    ))
                }
            </div>
        </div>
    );
};
// const mapStateToProps = (state, ownProps) => ({
//     collection: selectCollection(ownProps.match.params.collectionId)(state)
//     // This is necessary because unlike other selectors, this selector needs a part of the state depending on the URL parameter!
// })
export default CollectionPage;
