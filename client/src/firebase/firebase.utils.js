import firebase from "firebase/compat/app";
import "firebase/compat/auth"
import "firebase/compat/firestore"



const firebaseConfig = {
  apiKey: "AIzaSyC8XA5SDLm4-jpZn9BntRSVtYEyJEOaI3E",
  authDomain: "crwn-db-cfa20.firebaseapp.com",
  projectId: "crwn-db-cfa20",
  storageBucket: "crwn-db-cfa20.appspot.com",
  messagingSenderId: "463144255858",
  appId: "1:463144255858:web:c3b30fc6a42d05c95e5deb",
  measurementId: "G-7L18T87TPF"
};

export const createUserProfileDocument = async(userAuth, additionalData) => {
    if(!userAuth) return;

    const userRef = firestore.doc(`users/${userAuth.uid}`) // returns id and path of that user in firebase
    const snapShot = await userRef.get() //returns exists(true or false) and id
    if(!snapShot.exists) {
        const {displayName, email} = userAuth
        const createdAt = new Date()

        try {
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            })
        } catch(error) {
            console.log("error creating user", error.message)
        }
    }
    
    return userRef; //id and path
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// export 

export const addCollectionAndDocuments = async (collectionKey, objectsToAdd) => {
    const collectionRef = firestore.collection(collectionKey);
    console.log(collectionRef)

    const batch = firestore.batch();
    objectsToAdd.forEach(obj => {
        const newDocRef = collectionRef.doc();  //returns unique id for each object
        batch.set(newDocRef, obj)
    })

    return await batch.commit()
}

export const convertCollectionsSnapshotToMap = (collections) => {
    const transformedCollection = collections.docs.map(doc => {
        const {title, items} = doc.data();

        return {
            routeName: encodeURI(title.toLowerCase()),
            id: doc.id,
            title,
            items
        } 
    }); // [{routeName: hats, id: 1, title: hats, items: []}, {}, {}, {}, {}]

    return transformedCollection.reduce((accumulator, collection) => {
        accumulator[collection.title.toLowerCase()] = collection;
        return accumulator; // {sneakers: {…}}
    }, {}) // {sneakers: {…}, jackets: {…}, mens: {…}, hats: {…}, womens: {…}}
}

export const getCurrentUser = () => {
    return new Promise((resolve, reject) => {
        const unsubscribe = auth.onAuthStateChanged(userAuth => {
            unsubscribe();
            resolve(userAuth);
        }, reject)
    })
}
export const auth = firebase.auth();
export const firestore = firebase.firestore();

export const googleProvider = new firebase.auth.GoogleAuthProvider();
googleProvider.setCustomParameters({prompt: 'select_account'});
export const signInWithGoogle = () => auth.signInWithPopup(googleProvider);

export default firebase;