import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import CartItem from '../cart-item/cart-item.component';
import { selectCartItems } from '../../redux/cart/cart.selectors';
import { toggleCartHidden } from '../../redux/cart/cart.actions';
import { CartDropdownContainer, CartItemsContainer, EmptyMessageStyles, GoToCheckoutButton } from './cart-dropdown.styles';

const CartDropdown = () => {

    const dispatch = useDispatch()
    const cartItems = useSelector(selectCartItems)
    const history = useHistory();
    return (
        <CartDropdownContainer>
            <CartItemsContainer>
            {cartItems.length ? (
                cartItems.map(cartItem => (
                    <CartItem key={cartItem.id} item={cartItem}/>
                ))
            ): (
                <EmptyMessageStyles>Your cart is empty</EmptyMessageStyles>
            )}
            </CartItemsContainer>
            <GoToCheckoutButton onClick={() => {
                history.push('/checkout'); 
                dispatch(toggleCartHidden())
            }}>GO TO CHECKOUT</GoToCheckoutButton>
        </CartDropdownContainer>
    )
}

export default CartDropdown;
