import React, {useEffect} from 'react';
import './App.css';
import HomePage from './pages/homepage/homepage.component';
import {Switch, Route} from 'react-router-dom';
import Shop from './pages/shop/shop.component';
import Header from './components/header/header.component';
import SignInAndSignUp from './pages/sign-in-and-sign-up/sign-in-and-sign-up.component';
import {useSelector, useDispatch} from 'react-redux';
import { Redirect } from 'react-router';
import {selectCurrentUser} from './redux/user/user.selectors';
// import { selectCollectionsForPreview } from './redux/shop/shop.selectors';
import CheckoutPage from './pages/checkout/checkout.component';
import {checkUserSession} from "./redux/user/user.actions";
const App = () => {
  const dispatch = useDispatch()

  const currentUser = useSelector(selectCurrentUser)

  useEffect(() => {
    dispatch(checkUserSession());
    
  }, [dispatch])
    
  
  return (
    <div>
      <Header/>
      <Switch>
        <Route exact  path="/" component={HomePage}/>
        <Route path="/shop" component={Shop}/>
        <Route exact path="/signin" render={() => currentUser ? (<Redirect to="/"/>) : (<SignInAndSignUp/>)}/>
        <Route exact path="/checkout" component={CheckoutPage}/>
        
      </Switch>
    </div>
  );
  
}

// createStructuredSelector takes the values of its input-selectors and maps them to keys in an object:
// const mapStateToProps = createStructuredSelector({
//   currentUser: selectCurrentUser,
//   // collections:selectCollectionsForPreview
// })
// const mapDispatchToProps = dispatch => ({
//   checkUserSession: () => dispatch(checkUserSession())
// })
export default App;
