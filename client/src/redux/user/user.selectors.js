import { createSelector } from "reselect"; //A library for creating memoized "selector" functions
// createSelector generates memoized selector functions.
// If the generated selector is called multiple times, the output will only be recalculated when the extracted values have changed.
// The inputs and result are cached for later use.
// If the selector is called again with the same arguments, 
// the previously cached result is returned instead of recalculating a new result.

const selectUser = state => state.user;

export const selectCurrentUser = createSelector(
    [selectUser],
    (user) => user.currentUser
)

