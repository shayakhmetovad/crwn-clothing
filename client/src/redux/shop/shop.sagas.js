import {all, takeLatest, call, put} from 'redux-saga/effects';
// put instead of dispatch
import { firestore, convertCollectionsSnapshotToMap } from '../../firebase/firebase.utils';
import ShopActionTypes from './shop.types';
import { fetchCollectionsSuccess, fetchCollectionsFailure } from './shop.actions';

export function* fetchCollectionsAsync() {
    yield console.log("I am fired")
    try {
        const collectionRef = firestore.collection('collections'); //returns id:collections and path:collections
        const snapshot = yield collectionRef.get(); //returns array(5) and id of each collection 
        const collectionsMap = yield call(convertCollectionsSnapshotToMap, snapshot) // {sneakers: {…}, jackets: {…}, mens: {…}, hats: {…}, womens: {…}}
        yield put(fetchCollectionsSuccess(collectionsMap))
    } catch(error) {
        yield put(fetchCollectionsFailure(error.message))

    }

        // collectionRef.get().then(snapshot => {
        //     const collectionsMap = convertCollectionsSnapshotToMap(snapshot)
        //     console.log('collections', collectionsMap)
        //     dispatch(fetchCollectionsSuccess(collectionsMap));
        // })
        // .catch(err => 
        //     dispatch(fetchCollectionsFailure(err.message))
        // )
} 
export function* fetchCollectionsStart() {
    yield takeLatest(
        ShopActionTypes.FETCH_COLLECTIONS_START,
        fetchCollectionsAsync 
    )
}

export function* shopSagas() {
    yield all([
        call(fetchCollectionsStart)
    ])
}