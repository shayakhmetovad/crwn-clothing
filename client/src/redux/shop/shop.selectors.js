import { createSelector } from "reselect";
import memoize from 'lodash.memoize';

const selectShop = (state) => state.shop;

export const selectShopCollections = createSelector(
    [selectShop],
    shop => shop.collections 
);

export const selectCollectionsForPreview = createSelector(
    [selectShopCollections],
    collections => collections ? Object.keys(collections).map(key => collections[key]) : []
)

// export const selectCollection = collectionUrlParam =>
//     createSelector(
//         [selectShopCollections],
//         collections => collections.find(
//             collection => collection.id===COLLECTION_ID_MAP[collectionUrlParam]
//         )
//     )
export const selectCollection = memoize((collectionUrlParam) =>
    createSelector(
        [selectShopCollections],
        collections => collections ? collections[collectionUrlParam] : null
        )
    )

export const sellectIsCollectionFetching = createSelector(
    [selectShop],
    shop => shop.isFetching
)
export const sellectIsCollectionsLoaded = createSelector(
    [selectShop],
    shop => !!shop.collections
    // 0, null, undefined, false -> false
)